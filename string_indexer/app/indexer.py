
import os

from pyspark.ml.feature import StringIndexer
from xpresso.ai.core.data.pipeline.abstract_spark_pipeline_component import \
    AbstractSparkPipelineEstimator


class CustomStringIndexer(StringIndexer, AbstractSparkPipelineEstimator):
    
    def __init__(self, name, xpresso_run_name, inputCol=None, outputCol=None, handleInvalid='error', stringOrderType='frequencyDesc'):
        class_name = self.__class__.__name__
        print(f'In class: {class_name} component_name={name} with xpresso_run_name {xpresso_run_name}', flush=True)
        StringIndexer.__init__(self, inputCol=inputCol, outputCol=outputCol, handleInvalid=handleInvalid, stringOrderType=stringOrderType)
        AbstractSparkPipelineEstimator.__init__(self, name=name)
        self.name = name
        self.xpresso_run_name = xpresso_run_name

    def _fit(self, dataset):
        self.state = dataset
        self.start(self.xpresso_run_name)
        print(f"In {self.__class__.__name__} _fit, now calling super's _fit", flush=True)
        model = super()._fit(dataset)
        print(f'Completed component: {self.name}', flush=True)
        return model

class LabelIndexer(StringIndexer, AbstractSparkPipelineEstimator):
    
    def __init__(self, name, xpresso_run_name, inputCol=None, outputCol=None, handleInvalid='error', stringOrderType='frequencyDesc'):
        class_name = self.__class__.__name__
        print(f'In class: {class_name} component_name={name} with xpresso_run_name {xpresso_run_name}', flush=True)
        StringIndexer.__init__(self, inputCol=inputCol, outputCol=outputCol, handleInvalid=handleInvalid, stringOrderType=stringOrderType)
        AbstractSparkPipelineEstimator.__init__(self, name=name)
        self.name = name
        self.xpresso_run_name = xpresso_run_name
    
    def _fit(self, dataset):
        self.state = dataset
        self.start(self.xpresso_run_name)
        print(f"In {self.__class__.__name__} _fit, now calling super's _fit", flush=True)
        model = super()._fit(dataset)
        print(f'Completed component: {self.name}', flush=True)
        return model