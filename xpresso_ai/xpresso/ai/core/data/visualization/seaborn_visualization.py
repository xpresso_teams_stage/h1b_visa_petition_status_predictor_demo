"""Class for visualization of attribute metrics using seaborn, matplotlib"""

__all__ = ["SeabornVisualization"]
__author__ = ["Ashritha Goramane"]

import pandas as pd

from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    MultivariatePlotException
from xpresso.ai.core.commons.utils.constants import *
from xpresso.ai.core.data.exploration.data_type import DataType
from xpresso.ai.core.data.visualization import utils
from xpresso.ai.core.data.visualization.abstract_visualization import \
    PlotType, \
    AbstractVisualize
from xpresso.ai.core.data.visualization.report import UnivariateReport, \
    MultivariateReport, CombinedReport, ScatterReport, ReportParam
from xpresso.ai.core.data.visualization.seaborn import scatter, bar, box, pie, \
    density, heatmap, mosaic
from xpresso.ai.core.logging.xpr_log import XprLogger


class SeabornVisualization(AbstractVisualize):
    """
        Visualization class takes a automl object and provide functions
        to render plots on the metrics of attribute
        Args:
            dataset(:obj StructuredDataset): Structured dataset object on
            which visualization to be performed
    """

    def __init__(self, dataset):
        super().__init__(dataset)
        self.dataset = dataset
        self.attribute_info = dataset.info.attributeInfo
        self.metric = dataset.info.metrics
        self.logger = XprLogger()

    def render_univariate(self, attr_name=None, attr_type=None, plot_type=None,
                          output_format=utils.HTML,
                          output_path=REPORT_OUTPUT_PATH, report=False,
                          report_format=ReportParam.SINGLEPAGE.value,
                          file_name=None, target=None,
                          others_percent_threshold=None,
                          bar_plot_tail_threshold=None):
        """
        Renders univariate graphs for a particular attribute
        Args:
            attr_name (str): Specific attribute for which plotutils
                to be generated
            attr_type (:obj: `DataType`): Specific type whose
            plot_type has
            to be changed
            plot_type (:obj: `PlotType`): Specifies the type of plot
                to be generated
            output_format (str): html, png or pdf plots to be generated
            output_path(str): path where the html plots to be stored
            report (bool): if true generates a report
            report_format (:obj: `ReportParam`): whether the attribute
            distribution should be on same page or continued on
            different pages
            file_name(str): file name of report pdf
            target(str): Name of target variable in dataset
            others_percent_threshold(int): Optional threshold to club small
                categories in pie chart
            bar_plot_tail_threshold(int): Optional threshold to club tail
                in bar chart
        """
        if report_format.lower() not in [ReportParam.SINGLEPAGE.value,
                                         ReportParam.DOUBLEPAGE.value]:
            print("Incorrect report_format provided. Acceptable parameter "
                  "values are [single,double]")

        if report:
            output_format = "png"

        if target:
            self.target_variable_plot(target=target, attr_name=attr_name,
                                      output_format=output_format)
        self.univariate_plot(attr_name, attr_type, plot_type, output_format,
                             others_percent_threshold=others_percent_threshold,
                             bar_plot_tail_threshold=bar_plot_tail_threshold)
        if report:
            UnivariateReport(dataset=self.dataset).create_report(
                output_path=output_path,
                report_format=report_format,
                file_name=file_name)

    def univariate_plot(self, attr_name=None, attr_type=None, plot_type=None,
                        output_format=utils.HTML,
                        others_percent_threshold=None,
                        bar_plot_tail_threshold=None):
        """
        Helper function to generate univariate plots
        Args:
            attr_name(:str): optional attribute name to plot specific
                attribute plots
            attr_type(:str): optional attribute type to generate all plots
                for specific attribute type
            plot_type(:str): Plot type to be generated for corresponding
                attribute name or type
            output_format (str): html, png or pdf plots to be generated
            others_percent_threshold(int): Optional threshold to club small
                categories in pie chart
            bar_plot_tail_threshold(int): Optional threshold to club tail
                in bar chart
        """
        possible_plots = {
            DataType.NUMERIC.value: [PlotType.QUARTILE.value,
                                     PlotType.BAR.value],
            DataType.NOMINAL.value: [PlotType.PIE.value, PlotType.BAR.value],
            DataType.ORDINAL.value: [PlotType.PIE.value, PlotType.BAR.value],
            DataType.DATE.value: [PlotType.PIE.value, PlotType.BAR.value]
        }
        attr_type_plot_mapping = {
            DataType.NUMERIC.value: [PlotType.QUARTILE.value,
                                     PlotType.BAR.value],
            DataType.NOMINAL.value: [PlotType.PIE.value],
            DataType.ORDINAL.value: [PlotType.PIE.value],
            DataType.DATE.value: [PlotType.PIE.value]
        }
        is_plotted = False
        if plot_type is not None and attr_type is not None:
            if plot_type in possible_plots[attr_type]:
                attr_type_plot_mapping[attr_type] = [plot_type]
            else:
                self.logger.info(
                    "{} plot not possible for {} type. Hence plotting "
                    "default.".format(plot_type, attr_type))
        if plot_type is not None and attr_name is not None:
            attr_list = list(filter(lambda attr: attr.name == attr_name,
                                    self.info.attributeInfo))
            if not attr_list:
                self.logger.error(
                    "{} attribute doesn't exist".format(attr_name))
                return
            attr = attr_list[0]
            if plot_type in possible_plots[attr.type]:
                attr_type_plot_mapping[attr.type] = [plot_type]
            else:
                self.logger.info("{} plot not possible for {} attribute. "
                                 "Hence plotting default.".format(plot_type,
                                                                  attr_name))
        for attr in self.attribute_info:

            if (attr.name != attr_name and attr_name is not None) \
                    or (self.dataset.data[attr.name].isnull().all()):
                continue

            if attr.type == DataType.NUMERIC.value and \
                    PlotType.QUARTILE.value in \
                    attr_type_plot_mapping[attr.type]:
                field = "quartiles"
                is_plotted = self.plot(field, attr, PlotType.QUARTILE.value,
                                       output_format)
            if attr.type == DataType.NUMERIC.value and \
                    PlotType.BAR.value in attr_type_plot_mapping[attr.type]:
                field = "pdf"
                is_plotted = self.plot(field, attr, PlotType.BAR.value,
                                       output_format,
                                       bar_plot_tail_threshold=bar_plot_tail_threshold)

            if (
                    attr.type == DataType.NOMINAL.value or attr.type ==
                    DataType.ORDINAL.value) \
                    and PlotType.PIE.value in attr_type_plot_mapping[attr.type]:
                field = "freq_count"
                is_plotted = self.plot(field, attr, PlotType.PIE.value,
                                       output_format,
                                       others_percent_threshold=others_percent_threshold)

            if (
                    attr.type == DataType.NOMINAL.value or attr.type ==
                    DataType.ORDINAL.value) \
                    and PlotType.BAR.value in attr_type_plot_mapping[attr.type]:
                field = "freq_count"
                is_plotted = self.plot(field, attr, PlotType.BAR.value,
                                       output_format,
                                       bar_plot_tail_threshold=bar_plot_tail_threshold)

            if attr.type == DataType.DATE.value and \
                    PlotType.PIE.value in attr_type_plot_mapping[attr.type]:
                plot_key = ["day_count", "month_count", "year_count"]
                for field in plot_key:
                    is_plotted = self.plot(field, attr, PlotType.PIE.value,
                                           output_format,
                                           others_percent_threshold=others_percent_threshold)

            if attr.type == DataType.DATE.value and \
                    PlotType.BAR.value in attr_type_plot_mapping[attr.type]:
                plot_key = ["day_count", "month_count", "year_count"]
                for field in plot_key:
                    is_plotted = self.plot(field, attr, PlotType.BAR.value,
                                           output_format,
                                           bar_plot_tail_threshold=bar_plot_tail_threshold)

            if is_plotted is False:
                self.logger.error("Unable to plot for Attribute : ({},{}) "
                                  "with provided plot types parameter: {}"
                                  .format(attr.name, attr.type, plot_type))
        pass

    def plot(self, field=None, attr=None, plot_type=None,
             output_format=utils.HTML, others_percent_threshold=None,
             bar_plot_tail_threshold=None):
        """
        Renders specific plot for field in metrics depending upon the type
                of plot
        field(str): Metric field of the data to be plotted
        attr(:obj:AttributeInfo): Attribute of which plot to be generated
        plot_type (:obj: `PlotType`): Specifies the type of plot
            to be generated
        output_format (str): html, png or pdf plots to be generated
        others_percent_threshold(int): Optional threshold to club small
            categories in pie chart
        bar_plot_tail_threshold(int): Optional threshold to club tail
            in bar chart
        """

        if field in attr.metrics.keys():
            self.logger.info("{} plot for Attribute : ({},{},{})".format(
                plot_type, attr.name, attr.type, field))

        else:
            self.logger.info(
                "Attribute : {}, Field : {}. Unable to perform visualization"
                "Key not present in metric".format(attr.name, field))
            return False
        plot_file_name = "{}_{}".format(attr.name, plot_type)
        plot_title = plot_type + ' plot for ' + attr.name
        plot_title = plot_title.capitalize()
        input_1 = list()
        input_2 = list()
        if field == "quartiles":
            axes_labels = self.set_axes_labels(EMPTY_STRING, EMPTY_STRING)
            input_1 = list(attr.metrics[field])
        elif field in ["freq_count", "day_count", "month_count", "year_count"]:
            axes_labels = self.set_axes_labels(str(attr.name), "Count")
            input_1 = list(attr.metrics[field].keys())
            input_2 = list(attr.metrics[field].values())
        elif field == "pdf":
            axes_labels = self.set_axes_labels(str(attr.name),
                                               "Count({})".format(attr.name))
            input_1 = list(attr.metrics[field].keys())
            input_2 = list(attr.metrics[field].values())
        else:
            return False
        if plot_type == PlotType.QUARTILE.value:
            box.NewPlot(input_1, output_format=output_format,
                        plot_title=plot_title, file_name=plot_file_name,
                        axes_labels=axes_labels)
            return True
        elif plot_type == PlotType.PIE.value:
            pie.NewPlot(input_2, input_1, output_format=output_format,
                        plot_title=plot_title, file_name=plot_file_name,
                        threshold=others_percent_threshold)
            return True
        elif plot_type == PlotType.BAR.value:
            bar.NewPlot(input_1, input_2, output_format=output_format,
                        plot_title=plot_title, file_name=plot_file_name,
                        axes_labels=axes_labels,
                        threshold=bar_plot_tail_threshold)
            return True
        return False

    def target_variable_plot(self, target, attr_name=None,
                             output_format=utils.HTML):
        """
        Helper function to plot all combination of target variable plots
        Args:
            target(:str): Target variable name
            attr_name(:str): optional attribute name to plot specific
            attribute vs target plots
            output_format (str): html, png or pdf plots to be generated
        """
        attr_list = list(filter(lambda attr: attr.name == target,
                                self.attribute_info))
        if not attr_list:
            self.logger.error("{} attribute doesn't exist".format(target))
            return
        target_attr = attr_list[0]

        for attr in self.attribute_info:
            if (attr.name != attr_name
                and attr_name is not None) \
                    or (self.dataset.data[attr.name].isnull().all()) \
                    or attr.name == target:
                continue
            if target_attr.type in [DataType.NOMINAL.value,
                                    DataType.ORDINAL.value] \
                    and attr.type == DataType.NUMERIC.value:
                self.plot_target(attr, target_attr, PlotType.QUARTILE.value,
                                 output_format)
                self.plot_target(attr, target_attr, PlotType.DENSITY.value,
                                 output_format)
            elif target_attr.type in [DataType.NOMINAL.value,
                                      DataType.ORDINAL.value] \
                    and attr.type in [DataType.NOMINAL.value,
                                      DataType.ORDINAL.value]:
                self.plot_target(attr, target_attr, PlotType.BAR.value,
                                 output_format)
                self.plot_target(attr, target_attr, PlotType.MOSAIC.value,
                                 output_format)
            elif target_attr.type == DataType.NUMERIC.value \
                    and attr.type == DataType.NUMERIC.value:
                self.plot_target(attr, target_attr, PlotType.SCATTER.value,
                                 output_format)
            elif target_attr.type == DataType.NUMERIC.value \
                    and attr.type in [DataType.NOMINAL.value,
                                      DataType.ORDINAL.value]:
                self.plot_target(attr, target_attr, PlotType.QUARTILE.value,
                                 output_format)

    def plot_target(self, attribute, target, plot_type, output_format=None):
        """Helper function to plot specific target variable plot
        Args:
            target(:str): Target variable name
            attribute(:obj attribute_info): optional attribute to plot specific
                attribute vs target plots
            plot_type(:str): Specifies the type of plot
                to be generated
            output_format (str): html, png or pdf plots to be generated
        """
        input_1 = self.dataset.data[attribute.name]
        input_2 = self.dataset.data[target.name]
        plot_file_name = "{}_target".format(attribute.name, plot_type)
        plot_title = "{} plot for {}".format(plot_type,
                                             attribute.name).capitalize()

        if plot_type == PlotType.BAR.value:
            axes_labels = self.set_axes_labels(attribute.name,
                                               "Count({})".format(
                                                   attribute.name), target.name)
            bar.NewPlot(input_1, target=input_2, plot_title=plot_title,
                        file_name=plot_file_name, output_format=output_format,
                        axes_labels=axes_labels)
        elif plot_type == PlotType.SCATTER.value:
            axes_labels = self.set_axes_labels(attribute.name, target.name,
                                               target.name)
            scatter.NewPlot(input_1, input_2, target=True,
                            auto_render=True, plot_title=plot_title,
                            file_name=plot_file_name,
                            output_format=output_format,
                            axes_labels=axes_labels)
        elif plot_type == PlotType.QUARTILE.value:
            axes_labels = self.set_axes_labels(target.name,
                                               EMPTY_STRING,
                                               EMPTY_STRING)
            box.NewPlot(input_1, target=input_2, plot_title=plot_title,
                        file_name=plot_file_name, output_format=output_format,
                        axes_labels=axes_labels)
        elif plot_type == PlotType.DENSITY.value:
            axes_labels = self.set_axes_labels(attribute.name,
                                               "P({})".format(attribute.name),
                                               target.name)
            density.NewPlot(input_1, target=input_2, plot_title=plot_title,
                            file_name=plot_file_name,
                            output_format=output_format,
                            axes_labels=axes_labels)
        elif plot_type == PlotType.MOSAIC.value:
            axes_labels = self.set_axes_labels(attribute.name, target.name)
            mosaic.NewPlot(input_1, input_2, plot_title=plot_title,
                           file_name=plot_file_name,
                           output_format=output_format,
                           axes_labels=axes_labels)

    def render_multivariate(self, output_format=utils.HTML,
                            output_path=REPORT_OUTPUT_PATH,
                            report=False, file_name=None):
        """
        Renders multivariate graphs for spearman, pearson, chi-square
        correlation coefficient
        Args:
            output_format (str): html, png or pdf plots to be generated
            output_path(str): path where the html plots to be stored
            report (bool): if true generates a report
            file_name(str): file name of report pdf
        """
        if report:
            output_format = utils.PNG

        correlation = ["pearson", "spearman", "chi_square"]
        is_plotted = False
        for corr in correlation:
            if corr not in self.metric.keys():
                self.logger.info(
                    "Unable to do Multivariate Visualisation for  Field : {}. "
                    "Key not present in metric".format(corr))
                continue
            try:
                self.logger.info("{} plot for Correlation Type : {}".format(
                    PlotType.HEATMAP.value, corr))
                corr_df = pd.Series(list(self.metric[corr].values()),
                                    index=pd.MultiIndex.from_tuples(
                                        self.metric[corr].keys()))

                corr_df = corr_df.unstack()
                plot_file_name = corr
                heatmap.NewPlot(corr_df,
                                output_format=output_format,
                                plot_title=corr,
                                file_name=plot_file_name)
                is_plotted = True

            except MultivariatePlotException as e:
                self.logger.error("{}".format(e.message))

        if not is_plotted:
            self.logger.error("No key found. Unable to plot for multivariate "
                              "metrics")

        if report:
            # self.scatter(output_format=output_format)
            MultivariateReport(self.dataset).create_report(
                output_path=output_path,
                file_name=file_name)

    def render_all(self, output_format=utils.HTML,
                   output_path=REPORT_OUTPUT_PATH, report=False,
                   report_format=ReportParam.SINGLEPAGE.value, file_name=None,
                   target=None):
        """
        Renders all univariate and multivariate graphs
        Args:
            output_format (str): html, png or pdf plots to be generated
            output_path(str): path where the html plots or report to be stored
            report (bool): if true generates a report
            report_format (:obj: `ReportParam`): whether the attribute
            distribution should be on same page or continued on different pages
            file_name(str): file name of report pdf
            target(str): Name of target variable in dataset
        """

        if report:
            output_format = utils.PNG

        for attr in self.attribute_info:
            self.render_univariate(attr_name=attr.name,
                                   output_format=output_format, target=target)
        self.render_multivariate(output_format=output_format)

        self.scatter(output_format=output_format, target=target)

        if report:
            CombinedReport(dataset=self.dataset).create_report(
                output_path=output_path, file_name=file_name,
                report_format=report_format)

    def scatter(self, attribute_x=None, attribute_y=None,
                output_format=utils.HTML, file_name=None,
                output_path=REPORT_OUTPUT_PATH, report=False, target=False):
        """
        Renders scatter plots for the numeric attributes
        Args:
            attribute_x(str): x axis attribute
            attribute_y(str): y axis attribute
            output_format (str): html, png or pdf plots to be generated
            output_path(str): path where the html/pdf/png plots to be stored
            file_name(str): File name of the plot to be stored
            report (bool): if true generates a report
            target(str): Name of target variable in dataset
        """
        output_path_report = None
        if report:
            output_format = utils.PNG
            output_path_report = output_path
        if output_format == utils.HTML:
            output_path = utils.DEFAULT_PDF_PATH
        elif output_format == utils.PNG:
            output_path = utils.DEFAULT_IMAGE_PATH
        else:
            return

        numeric_attr = self.dataset.info.metrics["numeric_attributes"]
        if attribute_x is not None and attribute_x not in numeric_attr:
            self.logger.info("{} not in numeric attribute".format(attribute_x))
            return
        if attribute_y is not None and attribute_y not in numeric_attr:
            self.logger.info("{} not in numeric attribute".format(attribute_y))
            return

        if attribute_x is not None and attribute_y is not None:
            axes_labels = self.set_axes_labels(attribute_x, attribute_y)
            plot_file_name = "{}_scatter".format(attribute_x)
            plot_title = "Scatter plot for {} {}".format(attribute_x,
                                                         attribute_y)
            scatter.NewPlot(self.dataset.data[attribute_x],
                            self.dataset.data[attribute_y],
                            axes_labels=axes_labels, auto_render=True,
                            output_format=output_format,
                            file_name=plot_file_name, output_path=output_path,
                            plot_title=plot_title, target=target)
        elif attribute_x is not None:
            plot_file_name = "{}_scatter".format(attribute_x)
            plot_title = "Scatter plot for {}".format(attribute_x)
            attribute_y = list(set(numeric_attr) - {attribute_x})
            scatter.Join(self.dataset.data[attribute_x],
                         self.dataset.data[attribute_y],
                         output_format=output_format,
                         file_name=plot_file_name,
                         plot_title=plot_title, target=target)
        elif attribute_x is None and attribute_y is None:
            for attribute_x in numeric_attr:
                plot_file_name = "{}_scatter".format(attribute_x)
                plot_title = "Scatter plot for {}".format(attribute_x)
                attribute_y = list(set(numeric_attr) - {attribute_x})
                scatter.Join(self.dataset.data[attribute_x],
                             self.dataset.data[attribute_y],
                             file_name=plot_file_name,
                             output_format=output_format,
                             plot_title=plot_title, target=target)
        if report:
            ScatterReport(dataset=self.dataset).create_report(
                output_path=output_path_report,
                file_name=file_name)
