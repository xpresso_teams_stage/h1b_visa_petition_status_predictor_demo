class Error(Exception):
    pass


class AlluxioIPMissingInConfig(Error):
    pass


class AlluxioClientException(Error):
    pass


class AlluxioImportException(Error):
    pass


class AlluxioGetTXTException(Error):
    pass


class AlluxioGetCSVException(Error):
    pass


class AlluxioGetXLSXException(Error):
    pass


class AlluxioFileByteFormatReadException(Error):
    pass


class AlluxioPortMissingInConfig(Error):
    pass


class AlluxioFileNameMissing(Error):
    pass
