""" DBConnector class for connectivity to various databases """

__author__ = 'Shlok Chaudhari'
__all__ = 'connector'


import xpresso.ai.core.commons.utils.constants as constants
import xpresso.ai.core.data.connections.connector_exception as connector_exception
from xpresso.ai.core.data.connections.abstract_db import AbstractDBConnector
from xpresso.ai.core.data.connections.external.presto import client as presto
from xpresso.ai.core.logging.xpr_log import XprLogger


class DBConnector(AbstractDBConnector):
    """

    DataConnector class for connecting to the databases

    """

    def __init__(self):
        """

        __init__() here initializes the client object needed for
        interacting with datasource API

        """

        self.client = None

    def getlogger(self):
        """

        Xpresso logger built on top of python module

        :return: xpresso logging module

        """

        return XprLogger()

    def connect(self, config):
        """

        Connect method to establish client-side connection with a database

        :param config: A JSON object, input by the user, that states the table
                        to be imported

        :return: table: target table name (String object)
        :return: columns: names of columns required, as a comma-separated string
                    Put '*' in case of all columns. (String object)

        """

        self.client = presto.PrestoConnector(config.get(constants.DSN))
        table = config.get(constants.table)
        columns = config.get(constants.columns)
        return table, columns

    def import_files(self, config):
        """

        DBConnector does not support importing files

        :param config:

        :return: None

        """

        return None

    def import_dataframe(self, config):
        """

        Importing data from a user specified table in a database

        :param config: A JSON object, input by the user, that states the table
                        to be imported

        Usage:

            config = {
                    "DSN": "testing_sqlserver",
                    "table": "presto_test",
                    "columns": "*"
            }

        :return: data_frame: A DataFrame object of the required table

        """

        data_frame = None
        try:
            table, columns = self.connect(config)
            data_frame = self.client.import_data(table, columns)
            self.close()
            self.getlogger().info("Data imported from database.")
        except connector_exception.ConnectorImportFailed as exc:
            self.getlogger().exception("\nInput configurations might be wrong. "
                                       "Check input JSON object.\n\n%s" % str(exc))
        return data_frame

    def close(self):
        """

        Method to close connection to database

        :return: None

        """

        self.client.close()
