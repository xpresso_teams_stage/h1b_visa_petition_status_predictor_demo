
from pyspark.ml.feature import OneHotEncoderEstimator
from xpresso.ai.core.data.pipeline.abstract_spark_pipeline_component import \
    AbstractSparkPipelineEstimator


class CustomerOneHotEncoderEstimator(OneHotEncoderEstimator, AbstractSparkPipelineEstimator):

    def __init__(self, name, xpresso_run_name, inputCols=None, outputCols=None, handleInvalid='error', dropLast=True):
        class_name = self.__class__.__name__
        print(f'In class: {class_name} component_name={name} with xpresso_run_name {xpresso_run_name}', flush=True)
        OneHotEncoderEstimator.__init__(self, \
                                            inputCols=inputCols, \
                                            outputCols=outputCols, \
                                            handleInvalid=handleInvalid, \
                                            dropLast=dropLast)
        AbstractSparkPipelineEstimator.__init__(self, name=name)
        self.name = name
        self.xpresso_run_name = xpresso_run_name
    
    def _fit(self, dataset):
        self.state = dataset
        self.start(self.xpresso_run_name)
        print(f"In {self.__class__.__name__} _fit, now calling super's _fit", flush=True)
        model = super()._fit(dataset)
        print(f'Completed component: {self.name}', flush=True)
        return model